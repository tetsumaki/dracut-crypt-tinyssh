#!/bin/bash

CRYPTPID="$(ps -C cryptsetup -o pid=)"
CRYPTCMD="$(ps -p $CRYPTPID -o args --no-headers)"
$CRYPTCMD && kill $CRYPTPID
