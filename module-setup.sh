#!/bin/bash

check() {
	require_binaries ps tcpserver tinysshd || return 1

	if [ ! -f /etc/tinyssh/authorized_keys ] && [ ! -f /root/.ssh/authorized_keys ]; then
		return 1
	fi

	[[ -d /etc/tinyssh/sshkeydir ]] || tinysshd-makekey -q /etc/tinyssh/sshkeydir

	return 0
}

depends() {
	echo network
	return 0
}

install() {
	if [ -f /etc/tinyssh/authorized_keys ]; then
		inst_simple /etc/tinyssh/authorized_keys /root/.ssh/authorized_keys
	elif [ -f /root/.ssh/authorized_keys ]; then
		inst_simple /root/.ssh/authorized_keys /root/.ssh/authorized_keys
	fi

	inst_multiple ps tcpserver tinysshd
	inst_multiple /etc/tinyssh/sshkeydir/ed25519.pk /etc/tinyssh/sshkeydir/.ed25519.sk
	inst "$moddir/unlock.sh" /bin/unlock.sh
	inst_hook pre-udev 99 "$moddir/start.sh"
	inst_hook pre-pivot 05 "$moddir/stop.sh"
}
