#!/bin/bash

TINYSSHDPID="$(ps -C tinysshd -o pid=)"
TCPSERVERPID="$(ps -C tcpserver -o pid=)"
kill $TCPSERVERPID $TINYSSHDPID
ip addr flush label '*'
